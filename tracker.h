#ifndef TRACKER_H
#define TRACKER_H

// opencv imports
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/video/tracking.hpp>

// other imports
#include <vector>
#include <string>

// custom classes
#include "cvsettings.h"

using namespace cv;
using namespace std;

class Tracker
{
public:
    // constructor
    Tracker();

    // image io methods, gray images are set as well accordingly
    void setTmp(cv::Mat img);
    void setImg1(cv::Mat img);
    void setImg2(cv::Mat img);

    // get images
    cv::Mat getTmp();
    cv::Mat getImg1();
    cv::Mat getImg2();
    cv::Mat getImg1Gray();
    cv::Mat getImg2Gray();

    // set filename lists
    void setFileNameList(vector<std::string> flist);

    // struct that stores settings (not in private?)
    CVSettings* cvSettings = new CVSettings();

    // detect image corners
    void setTrackMarkers(int idxImage);

    // track images in sequence
    void Tracker::trackImages();

    // get coordinates
    std::vector<std::vector<cv::Point2f>> getCoords();

private:

    // store image path lists
    std::vector<std::string>* filenames = new std::vector<std::string>;

    // temporary picture
    cv::Mat* tmp = new cv::Mat;

    // picture holders between which the tracking will be performed
    cv::Mat* img1 = new cv::Mat;
    cv::Mat* img2 = new cv::Mat;
    cv::Mat* img1gray = new cv::Mat;
    cv::Mat* img2gray = new cv::Mat;

    // vector that stores coordinates of all points at each frame
    std::vector<std::vector<cv::Point2f>>* coords = new std::vector<std::vector<cv::Point2f>>;
};

#endif // TRACKER_H
