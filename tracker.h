#ifndef TRACKER_H
#define TRACKER_H

// opencv imports
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/video/tracking.hpp>

// other imports
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include<QFileDialog>
#include<QMessageBox>
#include<QtCore>

// custom classes
#include "cvsettings.h"
#include "kinematics.h"

using namespace std;

class Tracker
{
public:
    // constructor
    Tracker();

    // image io methods, gray images are set as well accordingly
    void setTmp(cv::Mat img);
    void setImg1(cv::Mat img);
    void setImg2(cv::Mat img);
    void setMask(cv::Point2f P1, cv::Point2f P2);

    // get images
    cv::Mat getTmp();
    cv::Mat getImg1();
    cv::Mat getImg2();
    cv::Mat getImg1Gray();
    cv::Mat getImg2Gray();
    cv::Mat getMask();

    // set filename lists
    void setFileNameList(vector<std::string> flist);

    // struct that stores settings (not in private?)
    CVSettings* cvSettings = new CVSettings();

    // detect image corners
    void setTrackMarkers();

    // track images in sequence
    void Tracker::trackImages();

    // get coordinates
    cvPoints getCoords();

    // start and stop frame
    void setStartFrame(int position);
    void setEndFrame(int position);
    int getStartFrame();
    int getEndFrame();

private:
    // check of tracker was run already
    bool wasTracked = false;

    // start and stop index for tracker
    int startFrame = 0;
    int endFrame   = 0;

    // store image path lists
    std::vector<std::string>* filenames = new std::vector<std::string>;

    // temporary picture
    cv::Mat* tmp = new cv::Mat;

    // picture holders between which the tracking will be performed
    cv::Mat* img1 = new cv::Mat;
    cv::Mat* img2 = new cv::Mat;
    cv::Mat* img1gray = new cv::Mat;
    cv::Mat* img2gray = new cv::Mat;

    // mask for corner detection
    cv::Mat* mask = new cv::Mat;

    // vector that stores coordinates of all points at each frame
    cvPoints* coords = new cvPoints;
};

#endif // TRACKER_H
