/********************************************************************************
** Form generated from reading UI file 'frmmain.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FRMMAIN_H
#define UI_FRMMAIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_frmMain
{
public:
    QAction *actionEdge_Detector;
    QAction *actionTracker;
    QAction *actionAdd_Mask;
    QAction *actionLoad_Image_Sequence;
    QAction *actionExport_Coordinates;
    QAction *actionExport_Strains;
    QAction *actionQuit;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QListWidget *listWidget;
    QSlider *sliderPos;
    QLabel *lblChosenFile;
    QPushButton *btn_TrackIt;
    QPushButton *btn_setMarkers;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuConfiguration;

    void setupUi(QMainWindow *frmMain)
    {
        if (frmMain->objectName().isEmpty())
            frmMain->setObjectName(QStringLiteral("frmMain"));
        frmMain->resize(271, 445);
        actionEdge_Detector = new QAction(frmMain);
        actionEdge_Detector->setObjectName(QStringLiteral("actionEdge_Detector"));
        actionTracker = new QAction(frmMain);
        actionTracker->setObjectName(QStringLiteral("actionTracker"));
        actionAdd_Mask = new QAction(frmMain);
        actionAdd_Mask->setObjectName(QStringLiteral("actionAdd_Mask"));
        actionLoad_Image_Sequence = new QAction(frmMain);
        actionLoad_Image_Sequence->setObjectName(QStringLiteral("actionLoad_Image_Sequence"));
        actionExport_Coordinates = new QAction(frmMain);
        actionExport_Coordinates->setObjectName(QStringLiteral("actionExport_Coordinates"));
        actionExport_Strains = new QAction(frmMain);
        actionExport_Strains->setObjectName(QStringLiteral("actionExport_Strains"));
        actionQuit = new QAction(frmMain);
        actionQuit->setObjectName(QStringLiteral("actionQuit"));
        centralWidget = new QWidget(frmMain);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        gridLayout->addWidget(listWidget, 5, 0, 1, 1);

        sliderPos = new QSlider(centralWidget);
        sliderPos->setObjectName(QStringLiteral("sliderPos"));
        sliderPos->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(sliderPos, 4, 0, 1, 1);

        lblChosenFile = new QLabel(centralWidget);
        lblChosenFile->setObjectName(QStringLiteral("lblChosenFile"));
        QFont font;
        font.setPointSize(8);
        lblChosenFile->setFont(font);
        lblChosenFile->setAutoFillBackground(true);

        gridLayout->addWidget(lblChosenFile, 3, 0, 1, 1);

        btn_TrackIt = new QPushButton(centralWidget);
        btn_TrackIt->setObjectName(QStringLiteral("btn_TrackIt"));

        gridLayout->addWidget(btn_TrackIt, 1, 0, 1, 1);

        btn_setMarkers = new QPushButton(centralWidget);
        btn_setMarkers->setObjectName(QStringLiteral("btn_setMarkers"));

        gridLayout->addWidget(btn_setMarkers, 0, 0, 1, 1);

        frmMain->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(frmMain);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 271, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuConfiguration = new QMenu(menuBar);
        menuConfiguration->setObjectName(QStringLiteral("menuConfiguration"));
        frmMain->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuConfiguration->menuAction());
        menuFile->addAction(actionLoad_Image_Sequence);
        menuFile->addAction(actionExport_Coordinates);
        menuFile->addAction(actionExport_Strains);
        menuFile->addAction(actionQuit);
        menuConfiguration->addAction(actionAdd_Mask);
        menuConfiguration->addAction(actionEdge_Detector);
        menuConfiguration->addAction(actionTracker);

        retranslateUi(frmMain);

        QMetaObject::connectSlotsByName(frmMain);
    } // setupUi

    void retranslateUi(QMainWindow *frmMain)
    {
        frmMain->setWindowTitle(QApplication::translate("frmMain", "frmMain", 0));
        actionEdge_Detector->setText(QApplication::translate("frmMain", "Edge Detector...", 0));
        actionEdge_Detector->setShortcut(QApplication::translate("frmMain", "Ctrl+K", 0));
        actionTracker->setText(QApplication::translate("frmMain", "Tracker...", 0));
        actionTracker->setShortcut(QApplication::translate("frmMain", "Ctrl+T", 0));
        actionAdd_Mask->setText(QApplication::translate("frmMain", "Add Mask...", 0));
        actionAdd_Mask->setShortcut(QApplication::translate("frmMain", "Ctrl+M", 0));
        actionLoad_Image_Sequence->setText(QApplication::translate("frmMain", "Load Sequence...", 0));
        actionLoad_Image_Sequence->setShortcut(QApplication::translate("frmMain", "Ctrl+I", 0));
        actionExport_Coordinates->setText(QApplication::translate("frmMain", "Export Coordinates...", 0));
        actionExport_Strains->setText(QApplication::translate("frmMain", "Export Strains...", 0));
        actionQuit->setText(QApplication::translate("frmMain", "Quit", 0));
        actionQuit->setShortcut(QApplication::translate("frmMain", "Ctrl+Q", 0));
        lblChosenFile->setText(QString());
        btn_TrackIt->setText(QApplication::translate("frmMain", "Track Sequence", 0));
        btn_setMarkers->setText(QApplication::translate("frmMain", "Set Markers", 0));
        menuFile->setTitle(QApplication::translate("frmMain", "File", 0));
        menuConfiguration->setTitle(QApplication::translate("frmMain", "Configuration", 0));
    } // retranslateUi

};

namespace Ui {
    class frmMain: public Ui_frmMain {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FRMMAIN_H
