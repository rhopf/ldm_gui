/********************************************************************************
** Form generated from reading UI file 'frmmain.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FRMMAIN_H
#define UI_FRMMAIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_frmMain
{
public:
    QAction *actionEdge_Detector;
    QAction *actionTracker;
    QAction *actionAdd_Mask;
    QAction *actionLoad_Image_Sequence;
    QAction *actionExport_Coordinates;
    QAction *actionExport_Strains;
    QAction *actionQuit;
    QAction *actionSave_Figure_1;
    QAction *actionSave_Figure_2;
    QWidget *centralWidget;
    QGridLayout *gridLayout_4;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QPushButton *btn_setImages;
    QLabel *lblChosenFile;
    QHBoxLayout *horizontalLayout;
    QPushButton *btn_setMask;
    QPushButton *btn_setMarkers;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_3;
    QPushButton *btn_TrackIt;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btn_plotStrains;
    QPushButton *pushButton;
    QLabel *lbl_sliderStart;
    QSlider *sliderStart;
    QLabel *lbl_sliderStop;
    QSlider *sliderStop;
    QLabel *lbl_sliderCurrent;
    QSlider *sliderPos;
    QListWidget *listWidget;
    QVBoxLayout *verticalLayout_3;
    QCustomPlot *customPlot;
    QCustomPlot *customPlot2;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuConfiguration;

    void setupUi(QMainWindow *frmMain)
    {
        if (frmMain->objectName().isEmpty())
            frmMain->setObjectName(QStringLiteral("frmMain"));
        frmMain->resize(875, 589);
        actionEdge_Detector = new QAction(frmMain);
        actionEdge_Detector->setObjectName(QStringLiteral("actionEdge_Detector"));
        actionTracker = new QAction(frmMain);
        actionTracker->setObjectName(QStringLiteral("actionTracker"));
        actionAdd_Mask = new QAction(frmMain);
        actionAdd_Mask->setObjectName(QStringLiteral("actionAdd_Mask"));
        actionLoad_Image_Sequence = new QAction(frmMain);
        actionLoad_Image_Sequence->setObjectName(QStringLiteral("actionLoad_Image_Sequence"));
        actionExport_Coordinates = new QAction(frmMain);
        actionExport_Coordinates->setObjectName(QStringLiteral("actionExport_Coordinates"));
        actionExport_Strains = new QAction(frmMain);
        actionExport_Strains->setObjectName(QStringLiteral("actionExport_Strains"));
        actionQuit = new QAction(frmMain);
        actionQuit->setObjectName(QStringLiteral("actionQuit"));
        actionSave_Figure_1 = new QAction(frmMain);
        actionSave_Figure_1->setObjectName(QStringLiteral("actionSave_Figure_1"));
        actionSave_Figure_2 = new QAction(frmMain);
        actionSave_Figure_2->setObjectName(QStringLiteral("actionSave_Figure_2"));
        centralWidget = new QWidget(frmMain);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_4 = new QGridLayout(centralWidget);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        btn_setImages = new QPushButton(groupBox);
        btn_setImages->setObjectName(QStringLiteral("btn_setImages"));

        verticalLayout->addWidget(btn_setImages);

        lblChosenFile = new QLabel(groupBox);
        lblChosenFile->setObjectName(QStringLiteral("lblChosenFile"));
        QFont font;
        font.setPointSize(8);
        lblChosenFile->setFont(font);
        lblChosenFile->setAutoFillBackground(true);

        verticalLayout->addWidget(lblChosenFile);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        btn_setMask = new QPushButton(groupBox);
        btn_setMask->setObjectName(QStringLiteral("btn_setMask"));

        horizontalLayout->addWidget(btn_setMask);

        btn_setMarkers = new QPushButton(groupBox);
        btn_setMarkers->setObjectName(QStringLiteral("btn_setMarkers"));

        horizontalLayout->addWidget(btn_setMarkers);


        verticalLayout->addLayout(horizontalLayout);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        verticalLayout_2->addWidget(groupBox);

        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        gridLayout_3 = new QGridLayout(groupBox_3);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        btn_TrackIt = new QPushButton(groupBox_3);
        btn_TrackIt->setObjectName(QStringLiteral("btn_TrackIt"));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        btn_TrackIt->setFont(font1);

        gridLayout_3->addWidget(btn_TrackIt, 0, 0, 1, 1);


        verticalLayout_2->addWidget(groupBox_3);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        btn_plotStrains = new QPushButton(groupBox_2);
        btn_plotStrains->setObjectName(QStringLiteral("btn_plotStrains"));

        horizontalLayout_2->addWidget(btn_plotStrains);

        pushButton = new QPushButton(groupBox_2);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout_2->addWidget(pushButton);


        gridLayout_2->addLayout(horizontalLayout_2, 0, 0, 1, 1);


        verticalLayout_2->addWidget(groupBox_2);

        lbl_sliderStart = new QLabel(centralWidget);
        lbl_sliderStart->setObjectName(QStringLiteral("lbl_sliderStart"));

        verticalLayout_2->addWidget(lbl_sliderStart);

        sliderStart = new QSlider(centralWidget);
        sliderStart->setObjectName(QStringLiteral("sliderStart"));
        sliderStart->setOrientation(Qt::Horizontal);

        verticalLayout_2->addWidget(sliderStart);

        lbl_sliderStop = new QLabel(centralWidget);
        lbl_sliderStop->setObjectName(QStringLiteral("lbl_sliderStop"));

        verticalLayout_2->addWidget(lbl_sliderStop);

        sliderStop = new QSlider(centralWidget);
        sliderStop->setObjectName(QStringLiteral("sliderStop"));
        sliderStop->setOrientation(Qt::Horizontal);

        verticalLayout_2->addWidget(sliderStop);

        lbl_sliderCurrent = new QLabel(centralWidget);
        lbl_sliderCurrent->setObjectName(QStringLiteral("lbl_sliderCurrent"));

        verticalLayout_2->addWidget(lbl_sliderCurrent);

        sliderPos = new QSlider(centralWidget);
        sliderPos->setObjectName(QStringLiteral("sliderPos"));
        sliderPos->setOrientation(Qt::Horizontal);

        verticalLayout_2->addWidget(sliderPos);

        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        verticalLayout_2->addWidget(listWidget);


        gridLayout_4->addLayout(verticalLayout_2, 0, 0, 1, 1);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        customPlot = new QCustomPlot(centralWidget);
        customPlot->setObjectName(QStringLiteral("customPlot"));

        verticalLayout_3->addWidget(customPlot);

        customPlot2 = new QCustomPlot(centralWidget);
        customPlot2->setObjectName(QStringLiteral("customPlot2"));

        verticalLayout_3->addWidget(customPlot2);


        gridLayout_4->addLayout(verticalLayout_3, 0, 1, 1, 1);

        gridLayout_4->setColumnStretch(1, 1);
        frmMain->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(frmMain);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 875, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuConfiguration = new QMenu(menuBar);
        menuConfiguration->setObjectName(QStringLiteral("menuConfiguration"));
        frmMain->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuConfiguration->menuAction());
        menuFile->addAction(actionLoad_Image_Sequence);
        menuFile->addAction(actionExport_Coordinates);
        menuFile->addAction(actionExport_Strains);
        menuFile->addAction(actionQuit);
        menuFile->addAction(actionSave_Figure_1);
        menuFile->addAction(actionSave_Figure_2);
        menuConfiguration->addAction(actionAdd_Mask);
        menuConfiguration->addAction(actionEdge_Detector);
        menuConfiguration->addAction(actionTracker);

        retranslateUi(frmMain);

        QMetaObject::connectSlotsByName(frmMain);
    } // setupUi

    void retranslateUi(QMainWindow *frmMain)
    {
        frmMain->setWindowTitle(QApplication::translate("frmMain", "ECM Tracker", 0));
        actionEdge_Detector->setText(QApplication::translate("frmMain", "Edge Detector...", 0));
        actionEdge_Detector->setShortcut(QApplication::translate("frmMain", "Ctrl+K", 0));
        actionTracker->setText(QApplication::translate("frmMain", "Tracker...", 0));
        actionTracker->setShortcut(QApplication::translate("frmMain", "Ctrl+T", 0));
        actionAdd_Mask->setText(QApplication::translate("frmMain", "Add Mask...", 0));
        actionAdd_Mask->setShortcut(QApplication::translate("frmMain", "Ctrl+M", 0));
        actionLoad_Image_Sequence->setText(QApplication::translate("frmMain", "Load Sequence...", 0));
        actionLoad_Image_Sequence->setShortcut(QApplication::translate("frmMain", "Ctrl+I", 0));
        actionExport_Coordinates->setText(QApplication::translate("frmMain", "Export Coordinates...", 0));
        actionExport_Strains->setText(QApplication::translate("frmMain", "Export Strains...", 0));
        actionQuit->setText(QApplication::translate("frmMain", "Quit", 0));
        actionQuit->setShortcut(QApplication::translate("frmMain", "Ctrl+Q", 0));
        actionSave_Figure_1->setText(QApplication::translate("frmMain", "Save Figure 1...", 0));
        actionSave_Figure_2->setText(QApplication::translate("frmMain", "Save Figure 2...", 0));
        groupBox->setTitle(QApplication::translate("frmMain", "Settings", 0));
        btn_setImages->setText(QApplication::translate("frmMain", "Load Images...", 0));
        lblChosenFile->setText(QString());
        btn_setMask->setText(QApplication::translate("frmMain", "Set Mask", 0));
        btn_setMarkers->setText(QApplication::translate("frmMain", "Detect Markers", 0));
        groupBox_3->setTitle(QApplication::translate("frmMain", "Tracking", 0));
        btn_TrackIt->setText(QApplication::translate("frmMain", "Track Sequence", 0));
        groupBox_2->setTitle(QApplication::translate("frmMain", "Plotting", 0));
        btn_plotStrains->setText(QApplication::translate("frmMain", "Plot Strains", 0));
        pushButton->setText(QApplication::translate("frmMain", "Clear Graphs", 0));
        lbl_sliderStart->setText(QApplication::translate("frmMain", "Start Frame", 0));
        lbl_sliderStop->setText(QApplication::translate("frmMain", "Last Frame", 0));
        lbl_sliderCurrent->setText(QApplication::translate("frmMain", "Current Frame", 0));
        menuFile->setTitle(QApplication::translate("frmMain", "File", 0));
        menuConfiguration->setTitle(QApplication::translate("frmMain", "Configuration", 0));
    } // retranslateUi

};

namespace Ui {
    class frmMain: public Ui_frmMain {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FRMMAIN_H
