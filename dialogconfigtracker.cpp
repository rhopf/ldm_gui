#include "dialogconfigtracker.h"
#include "ui_dialogconfigtracker.h"

void DialogConfigTracker::connectToMain(trackerSettings* trkSet)
{
    trkSettings = trkSet;
}

DialogConfigTracker::DialogConfigTracker(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogConfigTracker)
{
    ui->setupUi(this);

    // set text boxes
    ui->le_pyLevel->setText(QString::number(trkSettings->pyLevel));
    ui->le_blockSize->setText(QString::number(trkSettings->blockSize));
    ui->le_maxIter->setText(QString::number(trkSettings->termMaxCount));
    ui->le_maxEps->setText(QString::number(trkSettings->termEpsilon));
}

DialogConfigTracker::~DialogConfigTracker()
{
    delete ui;
}

void DialogConfigTracker::on_buttonBox_accepted()
{
    // set parameters
    trkSettings->pyLevel = ui->le_pyLevel->text().toInt();
    trkSettings->blockSize = ui->le_blockSize->text().toInt();
    trkSettings->termMaxCount = ui->le_maxIter->text().toInt();
    trkSettings->termEpsilon = ui->le_maxEps->text().toDouble();
}

void DialogConfigTracker::on_buttonBox_rejected()
{
    // set text boxes
    ui->le_pyLevel->setText(QString::number(trkSettings->pyLevel));
    ui->le_blockSize->setText(QString::number(trkSettings->blockSize));
    ui->le_maxIter->setText(QString::number(trkSettings->termMaxCount));
    ui->le_maxEps->setText(QString::number(trkSettings->termEpsilon));
}
