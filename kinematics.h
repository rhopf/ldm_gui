#ifndef KINEMATICS_H
#define KINEMATICS_H

// custom imports
#include <Eigen/Dense>
#include <vector>
#include <QtCore>
#include <math.h>

// opencv imports
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/video/tracking.hpp>

using namespace Eigen;

// typedefs
typedef std::vector<Vector2f> PointList;
typedef std::vector<cv::Point2f> cvPointList;
typedef std::vector<PointList> Points;
typedef std::vector<cvPointList> cvPoints;
typedef std::vector<Matrix2f> Gradients;
class Kinematics
{
public:
    // constructor overload
    Kinematics::Kinematics();
    Kinematics(cvPoints cvCoords);

    // set coordinates
    void setCoordinates(cvPoints cvCoords);

    // compute deformation maps
    void setDeformationGradients();
    void setPricipalStrains();

    // helper functions
    Vector2f getCenter(PointList points);

    // get fns
    PointList getStrains();
    int getNrPoints();
    int getNrFrames();
    Gradients getDeformationGradients();

    // get extremal strains
    float getMaximumStrainE1();
    float getMaximumStrainE2();
    float getMinimumStrainE1();
    float getMinimumStrainE2();

private:

    // vars
    int nFrames=0;
    int nPoints=0;

    Points* coords  = new Points;
    PointList* strains = new PointList;
    Gradients* F_images = new Gradients;
    Vector2f center = Vector2f(0, 0);

};

#endif // KINEMATICS_H
