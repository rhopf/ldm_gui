#ifndef FRMMAIN_H
#define FRMMAIN_H

// qt imports
#include <QMainWindow>
#include <QLabel>

// opencv imports
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/video/tracking.hpp>

// dialogs
#include "dialogconfigdetector.h"
#include "dialogconfigtracker.h"

// custom classes
#include "tracker.h"

// namespaces
using namespace std;
using namespace cv;

///////////////////////////////////////////////////////////////////////////////////////////////////
namespace Ui {
    class frmMain;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

// class definition
class frmMain : public QMainWindow {
    Q_OBJECT

public:
    explicit frmMain(QWidget *parent = 0);
    ~frmMain();

private slots:

    void on_btn_TrackIt_clicked();

    void on_actionEdge_Detector_triggered();

    void on_actionTracker_triggered();

    void on_sliderPos_sliderMoved(int position);

    void on_listWidget_currentRowChanged(int currentRow);

    void on_actionLoad_Image_Sequence_triggered();

    void on_btn_setMarkers_clicked();

private:
    Ui::frmMain *ui;

    // void displayInLabel(cv::Mat mat, QLabel *label);           // function prototype

    // make dialogs for settings
    DialogConfigDetector* diaCfgDet = new DialogConfigDetector(this);
    DialogConfigTracker* diaCfgTrk = new DialogConfigTracker(this);

    // store image files
    QStringList* filenames = new QStringList();
    Tracker* mainTracker = new Tracker();

};

#endif // FRMMAIN_H
