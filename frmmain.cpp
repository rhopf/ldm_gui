#include "frmmain.h"
#include "ui_frmmain.h"
#include <vector>

#include<QFileDialog>
#include<QtCore>

// constructor ////////////////////////////////////////////////////////////////////////////////////
frmMain::frmMain(QWidget *parent) : QMainWindow(parent), ui(new Ui::frmMain) {
    ui->setupUi(this);

    // connect main cvSettings to subform variables via pointers
    diaCfgDet->connectToMain(mainTracker->cvSettings->detSettings);
    diaCfgTrk->connectToMain(mainTracker->cvSettings->trkSettings);
}

// destructor /////////////////////////////////////////////////////////////////////////////////////
frmMain::~frmMain() {
    delete ui;
}
///////////////////////////////////////////////////////////////////////////////////////////////////


// WHERE TO PUT THIS?
void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
     if ( flags == (EVENT_FLAG_CTRLKEY + EVENT_FLAG_LBUTTON) )
     {
          qDebug() << "Left mouse button is clicked while pressing CTRL key - position (" << x << ", " << y << ")" << endl;
     }
     else if ( flags == (EVENT_FLAG_RBUTTON + EVENT_FLAG_SHIFTKEY) )
     {
          qDebug() << "Right mouse button is clicked while pressing SHIFT key - position (" << x << ", " << y << ")" << endl;
     }
     else if ( event == EVENT_MOUSEMOVE && flags == EVENT_FLAG_ALTKEY)
     {
          qDebug() << "Mouse is moved over the window while pressing ALT key - position (" << x << ", " << y << ")" << endl;
     }
}

//void frmMain::displayInLabel(cv::Mat mat, QLabel* label) {

//    // temp qimage
//    QImage qimg;

//    if(mat.channels() == 1) {                   // if grayscale image
//        qimg = QImage((uchar*)mat.data, (int) mat.cols, (int) mat.rows, (int) mat.step, QImage::Format_Indexed8);     // declare and return a QImage
//        label->setPixmap(QPixmap::fromImage(qimg));

//    } else if(mat.channels() == 3) {            // if 3 channel color image
//        cv::cvtColor(mat, mat, CV_BGR2RGB);     // invert BGR to RGB
//        qimg = QImage((uchar*)mat.data, (int) mat.cols, (int) mat.rows, (int) mat.step, QImage::Format_RGB888);       // declare and return a QImage
//        label->setPixmap(QPixmap::fromImage(qimg));

//    } else {
//        qDebug() << "in convertOpenCVMatToQtQImage, image was not 1 channel or 3 channel, should never get here";
//    }
//}

void frmMain::on_actionEdge_Detector_triggered()
{
    diaCfgDet->show();

    //qDebug() << cvSettings->detSettings.maxCorners;
}

void frmMain::on_actionTracker_triggered()
{
    diaCfgTrk->show();
}

void frmMain::on_sliderPos_sliderMoved(int position)
{
    if (filenames->size() > 0)
    {
        // open image
        cv::Mat im = cv::imread((*filenames)[position].toStdString());
        mainTracker->setTmp(im);

        // load it into cv frame
        cv::imshow("Images", mainTracker->getTmp());

        // highlight appropriate file in listview
        ui->listWidget->setCurrentRow(position);
    }
}

void frmMain::on_listWidget_currentRowChanged(int currentRow)
{
    // open image
    mainTracker->setTmp(cv::imread((*filenames)[currentRow].toStdString()));

    // load it into cv frame
    cv::imshow("Images", mainTracker->getTmp());

    // change slider
    ui->sliderPos->setValue(currentRow);
}

// LOAD IMAGE SEQUENCE
void frmMain::on_actionLoad_Image_Sequence_triggered()
{
    // load string list for images
    *filenames = QFileDialog::getOpenFileNames(this, tr("Image files"),
                                                          QDir::currentPath(),
                                                         tr("Bitmap files (*.bmp);;JPEG files (*.jpg);;PNG files (*.png);;TIF files (*.tif)") );

    // temp file list
    vector<std::string> flist;

    // if image paths got loaded
    if( !(filenames->isEmpty()) )
    {
        // write into list widget
        for (int i =0; i<filenames->count(); i++)
        {
            qDebug() << (*filenames)[i];
            flist.push_back((*filenames)[i].toLocal8Bit().constData());
            ui->listWidget->addItem((*filenames)[i].split("/").back());
        }

        // set slider limits
        ui->sliderPos->setMaximum(filenames->size()-1);

        // open image
        mainTracker->setTmp( cv::imread((*filenames)[0].toStdString()) );
    }

    // if unable to open image
    if (mainTracker->getTmp().empty()) {
        ui->lblChosenFile->setText("error: image not read from file");      // update lable with error message
        return;                                                             // and exit function
    }

    // add filenames to list
    mainTracker->setFileNameList(flist);

    // display images in window
    cv::namedWindow("Images", 1);

    //set the callback function for any mouse event
    cv::setMouseCallback("Images", CallBackFunc, NULL);

    cv::imshow("Images", mainTracker->getTmp());

    // if we get to this point image was opened successfully
    ui->lblChosenFile->setText((*filenames)[0]);
}

// find track markers
void frmMain::on_btn_setMarkers_clicked()
{
    if (filenames->size() > 0)
    {
        // set markers TODO: INDEX where the markers are to be placed
        mainTracker->setTrackMarkers(0);

        // set markers
        vector<vector<cv::Point2f>> points = mainTracker->getCoords();
        qDebug() << points.size();
        // display image
        cv::Mat img = mainTracker->getImg1();

        for(size_t i = 0; i < points[0].size(); i++ )
        {
            cv::circle(img, points[0][i], 2, cv::Scalar(0, 255, 0), -1 );
        }

        cv::imshow("Images", img);
    }
}

// track images
void frmMain::on_btn_TrackIt_clicked()
{
    if (filenames->size() > 0)
    {
        // tracker run
        mainTracker->trackImages();
    }

}
