#include "frmmain.h"
#include "ui_frmmain.h"

// constructor ////////////////////////////////////////////////////////////////////////////////////
frmMain::frmMain(QWidget *parent) : QMainWindow(parent), ui(new Ui::frmMain) {
    ui->setupUi(this);

    // connect main cvSettings to subform variables via pointers
    diaCfgDet->connectToMain(mainTracker->cvSettings->detSettings);
    diaCfgTrk->connectToMain(mainTracker->cvSettings->trkSettings);

    ui->customPlot->xAxis2->setVisible(true);
    ui->customPlot->xAxis2->setTickLabels(false);
    ui->customPlot->yAxis2->setVisible(true);
    ui->customPlot->yAxis2->setTickLabels(false);
    ui->customPlot2->xAxis2->setVisible(true);
    ui->customPlot2->xAxis2->setTickLabels(false);
    ui->customPlot2->yAxis2->setVisible(true);
    ui->customPlot2->yAxis2->setTickLabels(false);
}

// destructor /////////////////////////////////////////////////////////////////////////////////////
frmMain::~frmMain() {
    delete ui;
}
///////////////////////////////////////////////////////////////////////////////////////////////////

// gobal variables
cv::Point2f* P1 = new cv::Point2f;
cv::Point2f* P2 = new cv::Point2f;
bool clicked=true;
int graphCounter = 0;

// WHERE TO PUT THIS...AS A MEMBER OF CLASS?
void callbackSetCorners( int event, int x, int y, int f, void* )
{
    switch(event)
    {

        case  CV_EVENT_LBUTTONDOWN  :
                                        clicked=true;
                                        P1->x=x;
                                        P1->y=y;
                                        P2->x=x;
                                        P2->y=y;
                                        break;

        case  CV_EVENT_LBUTTONUP    :
                                        P2->x=x;
                                        P2->y=y;
                                        clicked=false;
                                        cv::destroyWindow("Drag Rectangular Area");
                                        break;

        case  CV_EVENT_MOUSEMOVE    :
                                        if(clicked){
                                        P2->x=x;
                                        P2->y=y;
                                        }
                                        break;

        default                     :   break;


    }
}

//void frmMain::displayInLabel(cv::Mat mat, QLabel* label) {

//    // temp qimage
//    QImage qimg;

//    if(mat.channels() == 1) {                   // if grayscale image
//        qimg = QImage((uchar*)mat.data, (int) mat.cols, (int) mat.rows, (int) mat.step, QImage::Format_Indexed8);     // declare and return a QImage
//        label->setPixmap(QPixmap::fromImage(qimg));

//    } else if(mat.channels() == 3) {            // if 3 channel color image
//        cv::cvtColor(mat, mat, CV_BGR2RGB);     // invert BGR to RGB
//        qimg = QImage((uchar*)mat.data, (int) mat.cols, (int) mat.rows, (int) mat.step, QImage::Format_RGB888);       // declare and return a QImage
//        label->setPixmap(QPixmap::fromImage(qimg));

//    } else {
//        qDebug() << "in convertOpenCVMatToQtQImage, image was not 1 channel or 3 channel, should never get here";
//    }
//}

void frmMain::on_actionEdge_Detector_triggered()
{
    diaCfgDet->show();
}

void frmMain::on_actionTracker_triggered()
{
    diaCfgTrk->show();
}

void frmMain::on_sliderPos_sliderMoved(int position)
{
    if (filenames->size() > 0)
    {
        // open image
        cv::Mat im = cv::imread((*filenames)[position].toStdString());

        // convert position into relative measure
        position = position - mainTracker->getStartFrame();

        // check if coordinates exist for that frame
        if (mainTracker->getCoords().size() > position)
        {

            // store coordinates locally
            vector<vector<cv::Point2f>> points = mainTracker->getCoords();

            // draw markers and lines
            if (position > 0)
            {
                for(size_t i = 0; i < points[position].size(); i++ )
                {
                    cv::circle(im, points[position][i], 2, cv::Scalar(0, 255, 0), -1 );
                    cv::line(im, points[position-1][i], points[position][i], cv::Scalar(0, 255, 255), 1, 8, 0);
                }
            }

            else
            {
                for(size_t i = 0; i < points[position].size(); i++ )
                {
                    cv::circle(im, points[position][i], 2, cv::Scalar(0, 255, 0), -1 );
                    //cv::line(im, points[position-1][i], points[position][i], cv::Scalar(0, 255, 255), 1, 8, 0);
                }
            }
        }

        // redraw image
        cv::imshow("Images", im);
        ui->listWidget->setCurrentRow(position + mainTracker->getStartFrame());

        // rename slider labels
        ui->lbl_sliderCurrent->setText("Current Frame: " + QString::number(position + mainTracker->getStartFrame()+1));
    }
}

// LOAD IMAGE SEQUENCE
void frmMain::on_actionLoad_Image_Sequence_triggered()
{
    // restart all except settings if files are loaded already
    if (filenames->size() > 0)
    {
        // reset all if there is a sequence existent
        delete mainTracker;
        delete filenames;
        mainTracker = new Tracker();
        filenames = new QStringList();

        // reconnect main cvSettings to subform variables via pointers
        diaCfgDet->connectToMain(mainTracker->cvSettings->detSettings);
        diaCfgTrk->connectToMain(mainTracker->cvSettings->trkSettings);

        // wipe slider and list==========
        ui->listWidget->clear();
    }

    // load string list for images
    *filenames = QFileDialog::getOpenFileNames(this, tr("Image files"),
                                                          QDir::currentPath(),
                                                         tr("Bitmap files (*.bmp);;JPG files (*.jpg);;PNG files (*.png);;TIF files (*.tif)") );

    // temp file list
    vector<std::string> flist;

    // if image paths got loaded
    if( !(filenames->isEmpty()) )
    {
        // write into list widget
        for (int i=0; i<filenames->count(); i++)
        {
            flist.push_back((*filenames)[i].toLocal8Bit().constData());
            ui->listWidget->addItem(QString::number(i+1) + ". " + (*filenames)[i].split("/").back());
        }

        // set slider limits
        ui->sliderPos->setMaximum(filenames->size()-1);
        ui->sliderStart->setMaximum(filenames->size()-1);
        ui->sliderStop->setMaximum(filenames->size()-1);
        ui->sliderStop->setValue(filenames->size()-1);

        // set start and stop frames
        mainTracker->setStartFrame(0);
        mainTracker->setEndFrame(filenames->size()-1);

        // set slider labels
        ui->lbl_sliderCurrent->setText("Current Frame: " + QString::number(1));
        ui->lbl_sliderStart->setText("Start Frame: " + QString::number(1));
        ui->lbl_sliderStop->setText("Last Frame: " + QString::number(filenames->count()));

        // open image
        mainTracker->setTmp( cv::imread((*filenames)[0].toStdString()) );
    }

    // if unable to open image
    if (mainTracker->getTmp().empty()) {
        ui->lblChosenFile->setText("error: image not read from file");      // update lable with error message
        return;                                                             // and exit function
    }

    // add filenames to list
    mainTracker->setFileNameList(flist);

    // display images in window
    cv::namedWindow("Images", 1);

    cv::imshow("Images", mainTracker->getTmp());

    // if we get to this point image was opened successfully
    ui->lblChosenFile->setText((*filenames)[0]);
}

// find track markers
void frmMain::on_btn_setMarkers_clicked()
{
    if (filenames->size() > 0)
    {
        // set markers TODO: INDEX where the markers are to be placed
        mainTracker->setTrackMarkers();

        // get markers
        vector<vector<cv::Point2f>> points = mainTracker->getCoords();

        // display image
        cv::Mat img = mainTracker->getImg1();

        for(size_t i = 0; i < points[0].size(); i++ )
        {
            cv::circle(img, points[0][i], 2, cv::Scalar(0, 255, 0), -1 );
        }

        cv::imshow("Images", img);
    }
}

// track images
void frmMain::on_btn_TrackIt_clicked()
{
    if (!(mainTracker->getCoords().empty()))
    {
        // tracker run
        mainTracker->trackImages();    

        // set kinematics
        mainKinematics = new Kinematics(mainTracker->getCoords());
        mainKinematics->setDeformationGradients();
        mainKinematics->setPricipalStrains();
        //QMessageBox::information(this, "Message", "Strains successfully computed.", "Ok");
    }
}

void frmMain::on_actionAdd_Mask_triggered()
{
    if (filenames->size() > 0)
    {
        // open image 0
        cv::Mat im = cv::imread((*filenames)[mainTracker->getStartFrame()].toStdString());
        cv::namedWindow("Drag Rectangular Area");
        cv::imshow("Drag Rectangular Area", im);

        //set the callback function for any mouse event
        cv::setMouseCallback("Drag Rectangular Area", callbackSetCorners, NULL);

        while(clicked)
        {
            cv::waitKey(20);
        }

        // set mask if possible
        if (P1->x==P2->x && P1->y==P2->y)
        {
            QMessageBox::warning(this, "Warning", "No mask was selected. Please left click-and-hold to drag a rectangle for the ROI.", "Ok");
            clicked=true;
            on_actionAdd_Mask_triggered();
        }
        else
        {
            mainTracker->setMask(*P1, *P2);
            clicked=true;
        }
    }
}

// export coordinates
void frmMain::on_actionExport_Coordinates_triggered()
{
    if (mainTracker->getCoords().size()>0)
    {
        // store coordinates locally
        std::vector<std::vector<cv::Point2f>> coords = mainTracker->getCoords();
        int nrFrames = (int) coords.size();
        int nrPoints = (int) coords[0].size();

        // save paths and file stream
        QString saveQFileX = QFileDialog::getSaveFileName(this, "Save X-Coordinates As...",
                                                          QDir::currentPath(),
                                                          "Text files (*.dat)");

        QString saveQFileY = QFileDialog::getSaveFileName(this, "Save Y-Coordinates As...",
                                                          QDir::currentPath(),
                                                          "Text files (*.dat)");
        std::string saveFileX = saveQFileX.toStdString();
        std::string saveFileY = saveQFileY.toStdString();
        std::ofstream myfilex, myfiley;
        myfilex.open(saveFileX);
        myfiley.open(saveFileY);

        // loop over all tracked frames
        for (int i=0; i<nrFrames; i++)
        {
            // loop over all points
            for (int j=0; j<nrPoints; j++)
            {
                myfilex << coords[i][j].x << " ";
                myfiley << coords[i][j].y << " ";
            }

            myfilex << "\n";
            myfiley << "\n";
        }

        // close stream
        myfilex.close();
        myfiley.close();
    }
    else
    {
        QMessageBox::warning(this, "Warning", "There are no coordinates to export.", "Ok");
    }
}

void frmMain::on_btn_setImages_clicked()
{
    frmMain::on_actionLoad_Image_Sequence_triggered();
}

void frmMain::on_btn_setMask_clicked()
{
    frmMain::on_actionAdd_Mask_triggered();
}

void frmMain::on_sliderStart_sliderMoved(int position)
{
    if (filenames->size() > 0)
    {
        // open image
        cv::Mat im = cv::imread((*filenames)[position].toStdString());

        // redraw image
        cv::imshow("Last Image", im);


        // set start frame
        mainTracker->setStartFrame(position);
        if (position > ui->sliderStop->value())
        {
            ui->sliderStop->setValue(position+1);
        }

        ui->sliderPos->setMinimum(position);

        // update slider labels
        ui->lbl_sliderCurrent->setText("Current Frame: " + QString::number(ui->sliderPos->value() + 1));
        ui->lbl_sliderStart->setText("First Frame: " + QString::number(ui->sliderStart->value() + 1));
        ui->lbl_sliderStop->setText("Last Frame: " + QString::number(ui->sliderStop->value() + 1));
    }
}

void frmMain::on_sliderStop_sliderMoved(int position)
{
    if (filenames->size() > 0)
    {
        // open image
        cv::Mat im = cv::imread((*filenames)[position].toStdString());

        // redraw image
        cv::imshow("First Image", im);


        // set last frame
        mainTracker->setEndFrame(position);
        if (position < ui->sliderStart->value())
        {
            ui->sliderStart->setValue(position-1);
        }

        ui->sliderPos->setMaximum(position);

        // update slider labels
        ui->lbl_sliderCurrent->setText("Current Frame: " + QString::number(ui->sliderPos->value() + 1));
        ui->lbl_sliderStart->setText("First Frame: " + QString::number(ui->sliderStart->value() + 1));
        ui->lbl_sliderStop->setText("Last Frame: " + QString::number(ui->sliderStop->value() + 1));
    }
}


void frmMain::on_actionExport_Strains_triggered()
{
    if (!(mainTracker->getCoords().empty()))
    {
        // store coordinates locally
        std::vector<Vector2f> strains = mainKinematics->getStrains();

        // save paths and file stream
        QString saveQFileX = QFileDialog::getSaveFileName(this, "Save Strains As...",
                                                          QDir::currentPath(),
                                                          "Text files (*.dat)");

        std::string saveFileX = saveQFileX.toStdString();
        std::ofstream myfilex;
        myfilex.open(saveFileX);

        // loop over all tracked frames
        for (int i=0; i<mainKinematics->getNrFrames(); i++)
        {
            myfilex << strains[i](0) << " " << strains[i](1) << "\n";
        }

        // close stream
        myfilex.close();
    }
    else
    {
        QMessageBox::warning(this, "Warning", "There are no strains to export.", "Ok");
    }
}

void frmMain::on_btn_plotStrains_clicked()
{
    if (mainTracker->getCoords().size()>1)
    {
        PointList strains = mainKinematics->getStrains();

        // containers
        QVector<double> x(mainKinematics->getNrFrames()), y1(mainKinematics->getNrFrames()), y2(mainKinematics->getNrFrames()); // initialize with entries 0..100

        // convert data
        for (int i=0; i<mainKinematics->getNrFrames(); ++i)
        {
          x[i] = (double) i;
          y1[i] = strains[i](1);
        }

        // create graph 1 in plot 1
        ui->customPlot->addGraph();
        ui->customPlot->graph(graphCounter)->setData(x, y1);
        ui->customPlot->graph(graphCounter)->setName("epsilon horiz.");
        ui->customPlot->graph(graphCounter)->setPen(QPen(QColor(0, 0, 255)));
        ui->customPlot->xAxis->setLabel("frame [nr]");
        ui->customPlot->yAxis->setLabel("strain [-]");
        graphCounter++;


        // convert data
        for (int i=0; i<mainKinematics->getNrFrames(); ++i)
        {
          x[i] = (double) i;
          y2[i] = strains[i](0);
        }
        // create graph 1 in plot 1
        ui->customPlot->addGraph();
        ui->customPlot->graph(graphCounter)->setData(x, y2);
        ui->customPlot->graph(graphCounter)->setName("epsilon vert.");
        ui->customPlot->graph(graphCounter)->setPen(QPen(QColor(255, 0, 0)));
        ui->customPlot->xAxis->setLabel("frame [nr]");
        ui->customPlot->yAxis->setLabel("strain [-]");
        graphCounter++;

        // set axes ranges, so we see all data:
        ui->customPlot->xAxis->setRange(0, mainKinematics->getNrFrames());
        ui->customPlot->yAxis->setRange(std::min(mainKinematics->getMinimumStrainE1(),mainKinematics->getMinimumStrainE2()),
                                        std::max(mainKinematics->getMaximumStrainE1(),mainKinematics->getMaximumStrainE2()));
        ui->customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
        ui->customPlot->legend->setVisible(true);
        QFont legendFont = font();  // start out with MainWindow's font..
        legendFont.setPointSize(9); // and make a bit smaller for legend
        ui->customPlot->legend->setFont(legendFont);
        ui->customPlot->legend->setBrush(QBrush(QColor(255,255,255,230)));

        ui->customPlot->replot();

        // create graph 1 in plot 2
        ui->customPlot2->addGraph();
        ui->customPlot2->graph(graphCounter/2-1)->setData(y1, y2);
        ui->customPlot2->graph(graphCounter/2-1)->setName("kinematics");
        ui->customPlot2->graph(graphCounter/2-1)->setPen(QPen(QColor(255, 0, 0)));
        ui->customPlot2->xAxis->setLabel("strain horiz. [-]]");
        ui->customPlot2->yAxis->setLabel("strain vert. [-]");

        // set axes ranges, so we see all data:
        ui->customPlot2->xAxis->setRange(mainKinematics->getMinimumStrainE2(), mainKinematics->getMaximumStrainE2());
        ui->customPlot2->yAxis->setRange(mainKinematics->getMinimumStrainE1(), mainKinematics->getMaximumStrainE1());

        ui->customPlot2->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
        ui->customPlot2->legend->setVisible(true);
        legendFont.setPointSize(9); // and make a bit smaller for legend
        ui->customPlot2->legend->setFont(legendFont);
        ui->customPlot2->legend->setBrush(QBrush(QColor(255,255,255,230)));

        ui->customPlot2->replot();
    }
}

void frmMain::on_btn_clearGraphs_clicked()
{
    ui->customPlot->clearGraphs();
    ui->customPlot->replot();
    graphCounter = 0;
}

void frmMain::on_actionSave_Figure_1_triggered()
{
    ui->customPlot->savePdf(QString("C:/Temp/test.pdf"));
}
