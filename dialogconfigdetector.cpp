#include "dialogconfigdetector.h"
#include "ui_dialogconfigdetector.h"
#include<QtCore>

// frame main
#include "frmmain.h"

void DialogConfigDetector::connectToMain(detectorSettings* detSet)
{
    detSettings = detSet;
}

DialogConfigDetector::DialogConfigDetector(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogConfigDetector)
{
    ui->setupUi(this);

    // set text boxes
    ui->le_maxCorners->setText(QString::number(detSettings->maxCorners));
    ui->le_minDist->setText(QString::number(detSettings->minDistance));
    ui->le_blockSize->setText(QString::number(detSettings->blockSize));
    ui->le_qualityLevel->setText(QString::number(detSettings->qualityLevel));
    ui->le_harrisK->setText(QString::number(detSettings->k));
    ui->checkBox_Harris->setChecked(detSettings->useHarrisDetector);
    ui->le_subpix->setText(QString::number(detSettings->subPixL));
    ui->le_maxIterET->setText(QString::number(detSettings->termMaxCount));
    ui->le_maxEpsET->setText(QString::number(detSettings->termEpsilon));

}

DialogConfigDetector::~DialogConfigDetector()
{
    delete ui;
}

void DialogConfigDetector::on_buttonBox_accepted()
{
    // set variable
    detSettings->maxCorners = ui->le_maxCorners->text().toInt();
    detSettings->minDistance = ui->le_minDist->text().toDouble();
    detSettings->blockSize = ui->le_blockSize->text().toInt();
    detSettings->qualityLevel = ui->le_qualityLevel->text().toDouble();
    detSettings->k = ui->le_harrisK->text().toDouble();
    detSettings->subPixL = ui->le_subpix->text().toInt();
    detSettings->useHarrisDetector = ui->checkBox_Harris->isChecked();
    detSettings->termMaxCount = ui->le_maxIterET->text().toInt();
    detSettings->termEpsilon = ui->le_maxEpsET->text().toDouble();

}

void DialogConfigDetector::on_buttonBox_rejected()
{
    // reset text boxes
    ui->le_maxCorners->setText(QString::number(detSettings->maxCorners));
    ui->le_minDist->setText(QString::number(detSettings->minDistance));
    ui->le_blockSize->setText(QString::number(detSettings->blockSize));
    ui->le_qualityLevel->setText(QString::number(detSettings->qualityLevel));
    ui->le_harrisK->setText(QString::number(detSettings->k));
    ui->checkBox_Harris->setChecked(detSettings->useHarrisDetector);
    ui->le_subpix->setText(QString::number(detSettings->subPixL));
    ui->le_maxIterET->setText(QString::number(detSettings->termMaxCount));
    ui->le_maxEpsET->setText(QString::number(detSettings->termEpsilon));
}
