#-------------------------------------------------
#
# Project created by QtCreator 2016-11-14T16:19:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = ldm_gui
TEMPLATE = app


SOURCES += main.cpp\
        frmmain.cpp \
    dialogconfigdetector.cpp \
    cvsettings.cpp \
    dialogconfigtracker.cpp \
    tracker.cpp \
    kinematics.cpp \
    qcustomplot.cpp

HEADERS  += frmmain.h \
    dialogconfigdetector.h \
    cvsettings.h \
    dialogconfigtracker.h \
    tracker.h \
    kinematics.h \
    qcustomplot.h

FORMS    += \
    frmmain.ui \
    dialogconfigdetector.ui \
    dialogconfigtracker.ui

INCLUDEPATH += C:\\lib\\opencv3\\opencv\\build\\include
INCLUDEPATH += C:\\lib\\eigen

LIBS += -LC:\\lib\\opencv3\\opencv\\build\\x64\\vc14\\lib \
    -lopencv_world310
