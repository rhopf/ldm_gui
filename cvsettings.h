#ifndef CVSETTINGS_H
#define CVSETTINGS_H

// opencv imports
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/video/tracking.hpp>

// namespaces
using namespace std;
// structs for different settings
struct detectorSettings
{
    // maxCorners – The maximum number of corners to return. If there are more corners
    // than that will be found, the strongest of them will be returned
    int maxCorners = 1000;

    // qualityLevel – Characterizes the minimal accepted quality of image corners;
    // the value of the parameter is multiplied by the by the best corner quality
    // measure (which is the min eigenvalue, see cornerMinEigenVal() ,
    // or the Harris function response, see cornerHarris() ).
    // The corners, which quality measure is less than the product, will be rejected.
    // For example, if the best corner has the quality measure = 1500,
    // and the qualityLevel=0.01 , then all the corners which quality measure is
    // less than 15 will be rejected.
    double qualityLevel = 0.001;

    // minDistance – The minimum possible Euclidean distance between the returned corners
    double minDistance = 2.;

    // mask – The optional region of interest. If the image is not empty (then it
    // needs to have the type CV_8UC1 and the same size as image ), it will specify
    // the region in which the corners are detected
    cv::Mat mask;

    // blockSize – Size of the averaging block for computing derivative covariation
    // matrix over each pixel neighborhood, see cornerEigenValsAndVecs()
    int blockSize = 3;

    // useHarrisDetector – Indicates, whether to use operator or cornerMinEigenVal()
    bool useHarrisDetector = false;

    // k – Free parameter of Harris detector
    double k = 0.04;

    // subpix win
    int subPixL = 10;

    // max iterations of detector
    int termMaxCount = 20;

    // max error norm
    double termEpsilon = 0.03;
};

struct trackerSettings
{
    // terminate after iterations and suffiecient eps
    int termType = cv::TermCriteria::COUNT|cv::TermCriteria::EPS; // CV_TERMCRIT_ITER, CV_TERMCRIT_EPS, or both

    // max iterations of LK
    int termMaxCount = 20;

    // max error norm
    double termEpsilon = 0.03;

    // pyramid level
    int pyLevel = 3;

    // track window block size
    int blockSize = 6;
};

// class
class CVSettings
{
public:
    CVSettings();

    detectorSettings* detSettings = new detectorSettings;
    trackerSettings* trkSettings = new trackerSettings;

};

#endif // CVSETTINGS_H
