#ifndef DIALOGCONFIGTRACKER_H
#define DIALOGCONFIGTRACKER_H

#include <QDialog>

// custom settings
#include "cvsettings.h"

namespace Ui {
class DialogConfigTracker;
}

class DialogConfigTracker : public QDialog
{
    Q_OBJECT

public:
    explicit DialogConfigTracker(QWidget *parent = 0);
    ~DialogConfigTracker();

    void connectToMain(trackerSettings* trkSet);

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::DialogConfigTracker *ui;

    // structs
    trackerSettings* trkSettings = new trackerSettings;
};

#endif // DIALOGCONFIGTRACKER_H
