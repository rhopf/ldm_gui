/********************************************************************************
** Form generated from reading UI file 'dialogconfigtracker.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGCONFIGTRACKER_H
#define UI_DIALOGCONFIGTRACKER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DialogConfigTracker
{
public:
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_3;
    QLabel *label;
    QLabel *label_2;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *le_blockSize;
    QLineEdit *le_pyLevel;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_3;
    QLabel *label_4;
    QVBoxLayout *verticalLayout;
    QLineEdit *le_maxIter;
    QLineEdit *le_maxEps;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *DialogConfigTracker)
    {
        if (DialogConfigTracker->objectName().isEmpty())
            DialogConfigTracker->setObjectName(QStringLiteral("DialogConfigTracker"));
        DialogConfigTracker->setWindowModality(Qt::ApplicationModal);
        DialogConfigTracker->resize(309, 256);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DialogConfigTracker->sizePolicy().hasHeightForWidth());
        DialogConfigTracker->setSizePolicy(sizePolicy);
        gridLayout_4 = new QGridLayout(DialogConfigTracker);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        groupBox_2 = new QGroupBox(DialogConfigTracker);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label = new QLabel(groupBox_2);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_3->addWidget(label);

        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_3->addWidget(label_2);


        horizontalLayout->addLayout(verticalLayout_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        le_blockSize = new QLineEdit(groupBox_2);
        le_blockSize->setObjectName(QStringLiteral("le_blockSize"));

        verticalLayout_2->addWidget(le_blockSize);

        le_pyLevel = new QLineEdit(groupBox_2);
        le_pyLevel->setObjectName(QStringLiteral("le_pyLevel"));

        verticalLayout_2->addWidget(le_pyLevel);


        horizontalLayout->addLayout(verticalLayout_2);

        horizontalLayout->setStretch(0, 3);
        horizontalLayout->setStretch(1, 1);

        gridLayout_2->addLayout(horizontalLayout, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox_2, 0, 0, 1, 1);

        groupBox = new QGroupBox(DialogConfigTracker);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout_4->addWidget(label_3);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout_4->addWidget(label_4);


        horizontalLayout_2->addLayout(verticalLayout_4);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        le_maxIter = new QLineEdit(groupBox);
        le_maxIter->setObjectName(QStringLiteral("le_maxIter"));

        verticalLayout->addWidget(le_maxIter);

        le_maxEps = new QLineEdit(groupBox);
        le_maxEps->setObjectName(QStringLiteral("le_maxEps"));

        verticalLayout->addWidget(le_maxEps);


        horizontalLayout_2->addLayout(verticalLayout);

        horizontalLayout_2->setStretch(0, 3);
        horizontalLayout_2->setStretch(1, 1);

        gridLayout->addLayout(horizontalLayout_2, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox, 1, 0, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 0, 0, 1, 1);

        buttonBox = new QDialogButtonBox(DialogConfigTracker);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout_4->addWidget(buttonBox, 1, 0, 1, 1);


        retranslateUi(DialogConfigTracker);
        QObject::connect(buttonBox, SIGNAL(accepted()), DialogConfigTracker, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DialogConfigTracker, SLOT(reject()));

        QMetaObject::connectSlotsByName(DialogConfigTracker);
    } // setupUi

    void retranslateUi(QDialog *DialogConfigTracker)
    {
        DialogConfigTracker->setWindowTitle(QApplication::translate("DialogConfigTracker", "Tracker Settings", 0));
        groupBox_2->setTitle(QApplication::translate("DialogConfigTracker", "Lucas-Kanade Set-Up", 0));
        label->setText(QApplication::translate("DialogConfigTracker", "Block Size [px]:", 0));
        label_2->setText(QApplication::translate("DialogConfigTracker", "Pyramid Level [1-8]:", 0));
        groupBox->setTitle(QApplication::translate("DialogConfigTracker", "Termiation Criteria", 0));
        label_3->setText(QApplication::translate("DialogConfigTracker", "Max. Iterations:", 0));
        label_4->setText(QApplication::translate("DialogConfigTracker", "Max. Epsilon:", 0));
    } // retranslateUi

};

namespace Ui {
    class DialogConfigTracker: public Ui_DialogConfigTracker {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGCONFIGTRACKER_H
