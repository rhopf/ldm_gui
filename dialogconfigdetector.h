#ifndef DIALOGCONFIGDETECTOR_H
#define DIALOGCONFIGDETECTOR_H

#include <QDialog>

// custom classes
#include "cvsettings.h"

namespace Ui {
class DialogConfigDetector;
}

class DialogConfigDetector : public QDialog
{
    Q_OBJECT

public:
    explicit DialogConfigDetector(QWidget *parent = 0);
    ~DialogConfigDetector();

    void connectToMain(detectorSettings* detSet);

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::DialogConfigDetector *ui;

    // structs that store settings
    detectorSettings* detSettings = new detectorSettings;
};

#endif // DIALOGCONFIGDETECTOR_H
