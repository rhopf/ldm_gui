#include "tracker.h"
#include<QtCore>

Tracker::Tracker()
{

}

// get coordinates
cvPoints Tracker::getCoords()
{
    return *coords;
}

// set markers
void Tracker::setTrackMarkers()
{
    // check if there are any images
    if (!(filenames->empty()))
    {
        // set image to track
        setImg1(cv::imread((*filenames)[startFrame]));

        // vector for points
        cvPointList points;

        // shi tomasi settings
        int maxCorners = cvSettings->detSettings->maxCorners;
        double qualityLevel = cvSettings->detSettings->qualityLevel;
        double minDistance = cvSettings->detSettings->minDistance;

        // EDIT
        int blockSize = cvSettings->detSettings->blockSize;
        bool useHarrisDetector = cvSettings->detSettings->useHarrisDetector;
        double k = cvSettings->detSettings->k;
        int subPixL = cvSettings->detSettings->subPixL;
        cv::Size subPixWinSize(subPixL, subPixL);

        int maxIter = cvSettings->detSettings->termMaxCount;
        double maxEps = cvSettings->detSettings->termEpsilon;

        // termination criteria
        cv::TermCriteria termcrit(cv::TermCriteria::COUNT|cv::TermCriteria::EPS, maxIter, maxEps);

        // shi tomasi feature detector
        cv::goodFeaturesToTrack(*img1gray, points, maxCorners, qualityLevel, minDistance, *mask, blockSize, useHarrisDetector, k );
        cv::cornerSubPix(*img1gray, points, subPixWinSize, cv::Size(-1,-1), termcrit);

        // reset coordinates and add corners
        delete coords;
        coords = new cvPoints;
        coords->push_back(points);
    }
}

// track all images
void Tracker::trackImages()
{
     // check that images are loaded and coords array is initialized
    if(!(filenames->empty()) && !(coords->empty()))
    {
        // temporary vars
        cvPointList oldPoints, nextPoints, tmpPoints;
        vector<uchar> status;
        vector<float> err;

        // termination criteria
        cv::TermCriteria termcrit(cv::TermCriteria::COUNT|cv::TermCriteria::EPS,
                                  cvSettings->trkSettings->termMaxCount,
                                  cvSettings->trkSettings->termEpsilon);

        // block size of the tracker
        cv::Size winSize(cvSettings->trkSettings->blockSize,
                         cvSettings->trkSettings->blockSize);

        // check if previous tracking exists
        if (wasTracked)
        {
            // reset track markers
            tmpPoints = (*coords)[0];
            delete coords;
            coords = new cvPoints;
            coords->push_back(tmpPoints);
        }

        // make new container for tracking animation
        cv::namedWindow("Tracking...");

        // loop over all images
        for(int i=startFrame; i<endFrame; i++)
        {
            // set images
            setImg1(cv::imread((*filenames)[i]));
            setImg2(cv::imread((*filenames)[i+1]));

            // set previous points
            oldPoints = coords->back();

            // track here by looping over all images
            cv::calcOpticalFlowPyrLK(*img1gray, *img2gray, oldPoints, nextPoints,
                                     status, err, winSize, cvSettings->trkSettings->pyLevel,
                                     termcrit, 0, 0.001);

            // display next image with trackers
            for(size_t i = 0; i < nextPoints.size(); i++ )
            {
                cv::circle(*img2, nextPoints[i], 2, cv::Scalar(0, 255, 0), -1 );
                cv::line(*img2, oldPoints[i], nextPoints[i], cv::Scalar(0, 255, 255), 1, 8, 0);
            }

            // budu
            cv::imshow("Tracking...", *img2);
            cv::waitKey(1);
            //cv::updateWindow("Tracking...");

            // append points
            coords->push_back(nextPoints);

        }

        // ensure flag
        wasTracked = true;

        cv::destroyWindow("Tracking...");
    }
}


// set file name list
void Tracker::setFileNameList(std::vector<std::string> flist)
{
    *filenames = flist;
    // set black mask when files loaded

    cv::Mat im = cv::imread((*filenames)[0]);

    // black image with same dimensions
    cv::Mat img = cv::Mat::zeros(im.size(), CV_8UC1);
    img.setTo(cv::Scalar::all(255));

    *mask = img;
}

// set current picture
void Tracker::setImg1(cv::Mat img)
{
    // colored image
    *img1 = img;

    // gray image
    cv::cvtColor(img, *img1gray, CV_BGR2GRAY);
}

// set deformed picture
void Tracker::setImg2(cv::Mat img)
{
    // colored image
    *img2 = img;

    // gray image
    cv::cvtColor(img, *img2gray, CV_BGR2GRAY);
}

// set temp picture
void Tracker::setTmp(cv::Mat img)
{
    // colored image only
    *tmp = img;
}

// set temp picture
void Tracker::setMask(cv::Point2f P1, cv::Point2f P2)
{
    // load first image
    cv::Mat im = cv::imread((*filenames)[startFrame]);

    // black image with same dimensions
    cv::Mat img = cv::Mat::zeros(im.size(), CV_8UC1);

    // set ROI
    cv::Rect ROI;
    // case P1 top left
    if (P1.x < P2.x && P1.y < P2.y)
    {
        ROI = cv::Rect(P1.x, P1.y, P2.x-P1.x, P2.y-P1.y);
        img(ROI).setTo(cv::Scalar::all(255));
    }

    // case P1 bottom left
    else if (P1.x < P2.x && P1.y > P2.y)
    {
        ROI = cv::Rect(P1.x, P2.y, P2.x-P1.x, P1.y-P2.y);
        img(ROI).setTo(cv::Scalar::all(255));
    }

    // case P1 bottom right
    else if (P1.x > P2.x && P1.y > P2.y)
    {
        ROI = cv::Rect(P2.x, P2.y, P1.x-P2.x, P1.y-P2.y);
        img(ROI).setTo(cv::Scalar::all(255));
    }

    // case P1 top right
    else
    {
        ROI = cv::Rect(P2.x, P1.y, P1.x-P2.x, P2.y-P1.y);
        img(ROI).setTo(cv::Scalar::all(255));
    }

    cv::Mat croppedImage = im(ROI);
    cv::imshow("Track Area", croppedImage);
    *mask = img;
}

// get methods
cv::Mat Tracker::getImg1()
{
    return *img1;
}

cv::Mat Tracker::getImg2()
{
    return *img1gray;
}

cv::Mat Tracker::getImg1Gray()
{
    return *img2;
}

cv::Mat Tracker::getImg2Gray()
{
    return *img2gray;
}

cv::Mat Tracker::getTmp()
{
    return *tmp;
}

cv::Mat Tracker::getMask()
{
    return *mask;
}

void Tracker::setStartFrame(int position)
{
    startFrame = position;
}

void Tracker::setEndFrame(int position)
{
    endFrame = position;
}

int Tracker::getStartFrame()
{
    return startFrame;
}

int Tracker::getEndFrame()
{
    return endFrame;
}
