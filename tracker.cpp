#include "tracker.h"
#include<QtCore>

Tracker::Tracker()
{

}

// get coordinates
std::vector<std::vector<cv::Point2f>> Tracker::getCoords()
{
    return *coords;
}

// set markers
void Tracker::setTrackMarkers(int idxImage)
{
    // check if there are any images
    if (filenames->size() >= idxImage)
    {
        // set image to track
        setImg1(cv::imread((*filenames)[idxImage]));

        // vector for points
        std::vector<cv::Point2f> points;

        // shi tomasi settings
        int maxCorners = cvSettings->detSettings->maxCorners;
        double qualityLevel = cvSettings->detSettings->qualityLevel;
        double minDistance = cvSettings->detSettings->minDistance;

        // EDIT
        cv::Mat mask = cvSettings->detSettings->mask;
        int blockSize = cvSettings->detSettings->blockSize;
        bool useHarrisDetector = cvSettings->detSettings->useHarrisDetector;
        double k = cvSettings->detSettings->k;
        int subPixL = cvSettings->detSettings->subPixL;
        cv::Size subPixWinSize(subPixL, subPixL);

        int maxIter = cvSettings->detSettings->termMaxCount;
        double maxEps = cvSettings->detSettings->termEpsilon;

        // termination criteria
        cv::TermCriteria termcrit(cv::TermCriteria::COUNT|cv::TermCriteria::EPS, maxIter, maxEps);

        // shi tomasi feature detector
        cv::goodFeaturesToTrack(*img1gray, points, maxCorners, qualityLevel, minDistance, mask, blockSize, useHarrisDetector, k );
        cv::cornerSubPix(*img1gray, points, subPixWinSize, cv::Size(-1,-1), termcrit);

        // reset coordinates and add corners
        delete coords;
        coords = new std::vector<std::vector<cv::Point2f>>;
        coords->push_back(points);
    }
}

// track all images
void Tracker::trackImages()
{
    if(!(filenames->empty()) && !(coords->empty()))
    {

    }
}


// set file name list
void Tracker::setFileNameList(std::vector<std::string> flist)
{
    *filenames = flist;
}

// set current picture
void Tracker::setImg1(cv::Mat img)
{
    // colored image
    *img1 = img;

    // gray image
    cv::cvtColor(img, *img1gray, CV_BGR2GRAY);
}

// set deformed picture
void Tracker::setImg2(cv::Mat img)
{
    // colored image
    *img2 = img;

    // gray image
    cv::cvtColor(img, *img2gray, CV_BGR2GRAY);
}

// set temp picture
void Tracker::setTmp(cv::Mat img)
{
    // colored image only
    *tmp = img;
}

// get methods
cv::Mat Tracker::getImg1()
{
    return *img1;
}

cv::Mat Tracker::getImg2()
{
    return *img1gray;
}

cv::Mat Tracker::getImg1Gray()
{
    return *img2;
}

cv::Mat Tracker::getImg2Gray()
{
    return *img2gray;
}

cv::Mat Tracker::getTmp()
{
    return *tmp;
}

