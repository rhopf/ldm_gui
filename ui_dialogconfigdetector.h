/********************************************************************************
** Form generated from reading UI file 'dialogconfigdetector.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGCONFIGDETECTOR_H
#define UI_DIALOGCONFIGDETECTOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DialogConfigDetector
{
public:
    QGridLayout *gridLayout;
    QDialogButtonBox *buttonBox;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupHarris;
    QGridLayout *gridLayout_2;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *checkBox_Harris;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_6;
    QLineEdit *le_harrisK;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_3;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_7;
    QLabel *label_8;
    QVBoxLayout *verticalLayout_5;
    QLineEdit *le_maxIterET;
    QSpacerItem *verticalSpacer_5;
    QLineEdit *le_maxEpsET;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_4;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *le_maxCorners;
    QSpacerItem *verticalSpacer;
    QLineEdit *le_minDist;
    QSpacerItem *verticalSpacer_2;
    QLineEdit *le_blockSize;
    QSpacerItem *verticalSpacer_3;
    QLineEdit *le_qualityLevel;
    QSpacerItem *verticalSpacer_4;
    QLineEdit *le_subpix;

    void setupUi(QDialog *DialogConfigDetector)
    {
        if (DialogConfigDetector->objectName().isEmpty())
            DialogConfigDetector->setObjectName(QStringLiteral("DialogConfigDetector"));
        DialogConfigDetector->setWindowModality(Qt::ApplicationModal);
        DialogConfigDetector->resize(299, 385);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DialogConfigDetector->sizePolicy().hasHeightForWidth());
        DialogConfigDetector->setSizePolicy(sizePolicy);
        gridLayout = new QGridLayout(DialogConfigDetector);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        buttonBox = new QDialogButtonBox(DialogConfigDetector);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 3, 0, 1, 1);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(8);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        groupHarris = new QGroupBox(DialogConfigDetector);
        groupHarris->setObjectName(QStringLiteral("groupHarris"));
        gridLayout_2 = new QGridLayout(groupHarris);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        checkBox_Harris = new QCheckBox(groupHarris);
        checkBox_Harris->setObjectName(QStringLiteral("checkBox_Harris"));

        verticalLayout_4->addWidget(checkBox_Harris);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(8);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_6 = new QLabel(groupHarris);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout_2->addWidget(label_6);

        le_harrisK = new QLineEdit(groupHarris);
        le_harrisK->setObjectName(QStringLiteral("le_harrisK"));

        horizontalLayout_2->addWidget(le_harrisK);

        horizontalLayout_2->setStretch(0, 3);
        horizontalLayout_2->setStretch(1, 1);

        verticalLayout_4->addLayout(horizontalLayout_2);


        gridLayout_2->addLayout(verticalLayout_4, 0, 0, 1, 1);


        verticalLayout_3->addWidget(groupHarris);


        gridLayout->addLayout(verticalLayout_3, 1, 0, 1, 1);

        groupBox = new QGroupBox(DialogConfigDetector);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout_3 = new QGridLayout(groupBox);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        verticalLayout_6->addWidget(label_7);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));

        verticalLayout_6->addWidget(label_8);


        horizontalLayout_3->addLayout(verticalLayout_6);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        le_maxIterET = new QLineEdit(groupBox);
        le_maxIterET->setObjectName(QStringLiteral("le_maxIterET"));

        verticalLayout_5->addWidget(le_maxIterET);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_5);

        le_maxEpsET = new QLineEdit(groupBox);
        le_maxEpsET->setObjectName(QStringLiteral("le_maxEpsET"));

        verticalLayout_5->addWidget(le_maxEpsET);


        horizontalLayout_3->addLayout(verticalLayout_5);

        horizontalLayout_3->setStretch(0, 3);
        horizontalLayout_3->setStretch(1, 1);

        gridLayout_3->addLayout(horizontalLayout_3, 0, 0, 1, 1);


        gridLayout->addWidget(groupBox, 2, 0, 1, 1);

        groupBox_2 = new QGroupBox(DialogConfigDetector);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        gridLayout_4 = new QGridLayout(groupBox_2);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout->addWidget(label_2);

        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout->addWidget(label_3);

        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout->addWidget(label_4);

        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));

        verticalLayout->addWidget(label_5);

        label = new QLabel(groupBox_2);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        le_maxCorners = new QLineEdit(groupBox_2);
        le_maxCorners->setObjectName(QStringLiteral("le_maxCorners"));

        verticalLayout_2->addWidget(le_maxCorners);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        le_minDist = new QLineEdit(groupBox_2);
        le_minDist->setObjectName(QStringLiteral("le_minDist"));

        verticalLayout_2->addWidget(le_minDist);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        le_blockSize = new QLineEdit(groupBox_2);
        le_blockSize->setObjectName(QStringLiteral("le_blockSize"));

        verticalLayout_2->addWidget(le_blockSize);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_3);

        le_qualityLevel = new QLineEdit(groupBox_2);
        le_qualityLevel->setObjectName(QStringLiteral("le_qualityLevel"));

        verticalLayout_2->addWidget(le_qualityLevel);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_4);

        le_subpix = new QLineEdit(groupBox_2);
        le_subpix->setObjectName(QStringLiteral("le_subpix"));

        verticalLayout_2->addWidget(le_subpix);


        horizontalLayout->addLayout(verticalLayout_2);

        horizontalLayout->setStretch(0, 3);
        horizontalLayout->setStretch(1, 1);

        gridLayout_4->addLayout(horizontalLayout, 0, 0, 1, 1);


        gridLayout->addWidget(groupBox_2, 0, 0, 1, 1);


        retranslateUi(DialogConfigDetector);
        QObject::connect(buttonBox, SIGNAL(accepted()), DialogConfigDetector, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DialogConfigDetector, SLOT(reject()));

        QMetaObject::connectSlotsByName(DialogConfigDetector);
    } // setupUi

    void retranslateUi(QDialog *DialogConfigDetector)
    {
        DialogConfigDetector->setWindowTitle(QApplication::translate("DialogConfigDetector", "Edge Detector Settings", 0));
        groupHarris->setTitle(QApplication::translate("DialogConfigDetector", "Harris Settings", 0));
        checkBox_Harris->setText(QApplication::translate("DialogConfigDetector", "Use Harris?", 0));
        label_6->setText(QApplication::translate("DialogConfigDetector", "Harris k-parameter:   ", 0));
        groupBox->setTitle(QApplication::translate("DialogConfigDetector", "Termination Criteria", 0));
        label_7->setText(QApplication::translate("DialogConfigDetector", "Max. Iterations: ", 0));
        label_8->setText(QApplication::translate("DialogConfigDetector", "Max. Epsilon:  ", 0));
        groupBox_2->setTitle(QApplication::translate("DialogConfigDetector", "General Settings", 0));
        label_2->setText(QApplication::translate("DialogConfigDetector", "Max. Corners [1-50000]:", 0));
        label_3->setText(QApplication::translate("DialogConfigDetector", "Min. Distance [px]:", 0));
        label_4->setText(QApplication::translate("DialogConfigDetector", "Block Size [px]:", 0));
        label_5->setText(QApplication::translate("DialogConfigDetector", "Quality Level [0-1]:", 0));
        label->setText(QApplication::translate("DialogConfigDetector", "Sub Pixel Window [px]:", 0));
    } // retranslateUi

};

namespace Ui {
    class DialogConfigDetector: public Ui_DialogConfigDetector {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGCONFIGDETECTOR_H
