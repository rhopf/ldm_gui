#include "kinematics.h"

// empty constructor
Kinematics::Kinematics()
{

}

// overloaded constructor
Kinematics::Kinematics(cvPoints cvCoords)
{
    if (!(cvCoords.empty()))
    {
        // vars
        nFrames = (int) cvCoords.size();
        nPoints = (int) cvCoords[0].size();

        // delete old coordinates
        if (!(coords==NULL))
        {
            delete coords;
            coords = new Points;
        }

        // fill points with eigen formatted coordinates
        PointList tmpPoints;
        for (int i_frame=0; i_frame<nFrames; i_frame++)
        {
            for (int i_point=0; i_point<nPoints; i_point++)
            {
                tmpPoints.push_back(Vector2f(cvCoords[i_frame][i_point].x,
                                             cvCoords[i_frame][i_point].y));
            }

            // add all points of current frame
            coords->push_back(tmpPoints);

            // reset temp points
            tmpPoints.clear();
        }
    }
}

// reset coordinates after retracking
void Kinematics::setCoordinates(cvPoints cvCoords)
{
    if (!(cvCoords.empty()))
    {
        // vars
        nFrames = (int) cvCoords.size();
        nPoints = (int) cvCoords[0].size();

        // delete old coordinates
        if (!(coords==NULL))
        {
            delete coords;
            coords = new Points;
        }

        // fill points with eigen formatted coordinates
        PointList tmpPoints;
        for (int i_frame=0; i_frame<cvCoords.size(); i_frame++)
        {
            for (int i_point=0; i_point<cvCoords[i_frame].size(); i_point++)
            {
                tmpPoints.push_back(Vector2f(cvCoords[i_frame][i_point].x,
                                             cvCoords[i_frame][i_point].y));
            }

            // add all points of current frame
            coords->push_back(tmpPoints);

            // reset temp points
            tmpPoints.clear();
        }
    }
}

// oh mighty deformation gradients
void Kinematics::setDeformationGradients()
{
    VectorXf transformParameters(2*nPoints);
    VectorXf rhs(2*nPoints);
    MatrixXf systemEqn(2*nPoints, 6);
    Matrix2f tempMap;


    // recompute center of reference
    center = getCenter((*coords)[0]);

    // shift all points

    for (int i=0; i<nFrames; i++)
    {
        for (int j=0; j<nPoints; j++)
        {
            (*coords)[i][j] = (*coords)[i][j]-center;
        }
    }

    // set reference coordinates
    PointList coords_ref = (*coords)[0];
    Points coords_def = *coords;

    // assemble system for least squares
    for (int i=0; i<nPoints; i++)
    {
        // first row
        systemEqn(2*i, 0)   = coords_ref[i](0);
        systemEqn(2*i, 1)   = coords_ref[i](1);
        systemEqn(2*i, 2)   = 0.0;
        systemEqn(2*i, 3)   = 0.0;
        systemEqn(2*i, 4)   = 1.0;
        systemEqn(2*i, 5)   = 0.0;

        // second row
        systemEqn(2*i+1, 0)   = 0.0;
        systemEqn(2*i+1, 1)   = 0.0;
        systemEqn(2*i+1, 2)   = coords_ref[i](0);
        systemEqn(2*i+1, 3)   = coords_ref[i](1);
        systemEqn(2*i+1, 4)   = 0.0;
        systemEqn(2*i+1, 5)   = 1.0;
    }

    // loop through all images and compute mapping
    for (int i=0; i<nFrames; i++)
    {
        // make rhs
        for (int j=0; j<nPoints; j++)
        {
            rhs(2*j)   = coords_def[i][j](0);
            rhs(2*j+1) = coords_def[i][j](1);
        }
        // solve least square system and append transform and discard RBM translations
        transformParameters =  systemEqn.jacobiSvd(ComputeThinU | ComputeThinV).solve(rhs);
        tempMap(0, 0) = transformParameters(0);
        tempMap(0, 1) = transformParameters(1);
        tempMap(1, 0) = transformParameters(2);
        tempMap(1, 1) = transformParameters(3);
        F_images->push_back(tempMap);
    }

}

// get principal strains
void Kinematics::setPricipalStrains()
{
    if (!(F_images->empty()))
    {
        if (!(strains==NULL))
        {
            delete strains;
            strains = new PointList;
        }

        for (int i=0; i<nFrames; i++)
        {
            // left cauchy green tensor and its eigenvalues
            Matrix2f B = (*F_images)[i]*(*F_images)[i].transpose();
            SelfAdjointEigenSolver<Matrix2f> eigensolver(B);
            Vector2f evals = eigensolver.eigenvalues();

            // set strains
            strains->push_back(Vector2f(sqrt(evals(0))-1,
                                        sqrt(evals(1))-1 ) );
        }
    }
}

float Kinematics::getMaximumStrainE1()
{
    float val=0;
    int axis=0;
    if (!(strains->empty()))
    {
        val = (*strains)[0](0);

        for (int i=0; i<nFrames; i++)
        {
            if((*strains)[i](axis)>val) val=(*strains)[i](axis);
        }
    }

    return val;
}

float Kinematics::getMaximumStrainE2()
{
    float val=0;
    int axis=1;
    if (!(strains->empty()))
    {
        val = (*strains)[0](0);

        for (int i=0; i<nFrames; i++)
        {
            if((*strains)[i](axis)>val) val=(*strains)[i](axis);
        }
    }

    return val;
}

float Kinematics::getMinimumStrainE1()
{
    float val=0;
    int axis=0;
    if (!(strains->empty()))
    {
        val = (*strains)[0](0);

        for (int i=0; i<nFrames; i++)
        {
            if((*strains)[i](axis)<val) val=(*strains)[i](axis);
        }
    }

    return val;
}

float Kinematics::getMinimumStrainE2()
{
    float val=0;
    int axis=1;
    if (!(strains->empty()))
    {
        val = (*strains)[0](0);

        for (int i=0; i<nFrames; i++)
        {
            if((*strains)[i](axis)<val) val=(*strains)[i](axis);
        }
    }

    return val;
}

// get fns
PointList Kinematics::getStrains()
{
    return *strains;
}


int Kinematics::getNrFrames()
{
    return nFrames;
}

int Kinematics::getNrPoints()
{
    return nPoints;
}

Gradients Kinematics::getDeformationGradients()
{
    return *F_images;
}

//helper functions ==================================
Vector2f Kinematics::getCenter(PointList points)
{
    Vector2f tmp = Vector2f(0, 0);

    for (int i=0; i<nPoints; i++)
    {
        tmp += points[i];
    }

    Vector2f center = tmp/nPoints;
    return center;
}
